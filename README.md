# Test REIGN

_solucion creada para el test de REIGN._

## Comenzando 🚀

### Pre-requisitos 📋


<a href="https://www.docker.com/products/docker-desktop">Docker</a> 
<a href="https://docs.docker.com/compose/install/">Docker Compose</a> 

 



## Despliegue 📦

```
git clone https://gitlab.com/croshyyyy/reign-test
cd reign-test
docker-compose up

```
abrir el navegador en http://localhost:3000